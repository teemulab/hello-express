# Hello-express

This project contains REST API with following features:

- Welcome endpoint
- Multiply endpoint

## Getting started

First install the dependencies

```sh
npm i
```

Start the REST API service:

`node app.js`

Or in the latest NODEJS version:

`node --watch app.js`


